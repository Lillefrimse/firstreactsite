/** @jsx React.DOM */
'use strict';

var React = require('react');
class Hello extends React.Component {
    render() {
        return (
          <div>
            <h1>Hello Line 123</h1>
            <h1>Hello Line 2345</h1>
          </div>)
    }
}

module.exports = Hello;
