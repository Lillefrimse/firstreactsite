/** @jsx React.DOM */
'use strict';
var React = require('react');
var ReactDOM = require('react-dom');
var Hello = require('./Hello.jsx');
require("../less/main.less"); //load less main file to compile all less.

class Index extends React.Component {
    render() {
        return (
            <div>
                <Hello />
            </div>
        )

    }
}
var MyComponentClass = React.createClass({
  render: function(){
    return (
      <h1>
        <h1>
          MyTestLine1000
        </h1>
        <h1>
          MyTestLine2000
        </h1>
      </h1>);
  }
});

ReactDOM.render(<MyComponentClass />, document.getElementById('content'));
RenderDOM.render(<Index />, document.getElementById('placeholder'));
